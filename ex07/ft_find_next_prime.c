/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_next_prime.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/25 15:20:57 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <stdio.h>

int	ft_is_prime(int nb)
{
	int	factor;

	factor = 5;
	if (nb == 2 || nb == 3 || nb == 2147483647)
		return (1);
	if (nb % 2 == 0 || nb % 3 == 0 || nb <= 1)
		return (0);
	while (factor * factor <= nb)
	{
		if (nb % factor == 0)
			return (0);
		factor += 2;
		if (nb % factor == 0)
			return (0);
		factor += 4;
	}
	return (1);
}

int	ft_find_next_prime(int nb)
{
	while (1 == 1)
	{
		if (ft_is_prime(nb) == 1)
			return (nb);
		nb++;
	}
}

/*int	main(void)
{
	printf("%d\n", ft_find_next_prime(2147483643));
}*/
