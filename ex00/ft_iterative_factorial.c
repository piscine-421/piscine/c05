/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/25 15:21:10 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <stdio.h>

int	ft_iterative_factorial(int nb)
{
	int	nb2;

	nb2 = nb;
	if (nb < 0)
		return (0);
	if (nb == 0)
		nb = 1;
	while (nb2 > 1)
	{
		nb2--;
		nb *= nb2;
	}
	return (nb);
}

/*int	main(void)
{
	printf("%d\n", ft_iterative_factorial(0));
}*/
